### STAGE 1: Build ###
FROM node:alpine3.11 AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run prod
### STAGE 2: Run ###
FROM nginx:alpine
COPY --from=build /usr/src/app/dist/angular-docker-test /usr/share/nginx/html
