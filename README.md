# AngularDockerTest

This project is a POC for building and deploying an Angular app in a Docker image based on Nginx 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Building the Docker image

Run `npm run docker-build` to build a docker image named angular-docker-test

## Running Docker image

Run `npm run docker-run` to run the image. The app can be access on `http://localhost:8888`.